unit MainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Generics.Collections,
  Vcl.ComCtrls;

type

  TFormMain = class(TForm)
    Label1: TLabel;
    Memo1: TMemo;
    TreeView1: TTreeView;
    Edit1: TEdit;
    Button1: TButton;
    Edit2: TEdit;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure Button2Click(Sender: TObject);
  private

  public

  end;

var
  FormMain: TFormMain;

implementation
uses XMLObject;

var
  Root: TXMLObject;

{$R *.dfm}

procedure TFormMain.Button1Click(Sender: TObject);
begin
  TreeView1.Items.Clear;
  if Root <> nil then
    Root.Free;

  Root := TXMLObject.Create;
  Root.ParsXML(Memo1.Text);
  Root.SendToTreeView(TreeView1);
end;

procedure TFormMain.Button2Click(Sender: TObject);
begin
  ShowMessage(Root.GetValue(Edit2.Text));
end;

procedure TFormMain.TreeView1Change(Sender: TObject; Node: TTreeNode);
var
  s: TTreeNode;
  names, GetString: string;
begin
  names := '';
  s := TreeView1.Selected;

  while s <> nil do
  begin
    names := s.Text + '|' + names;
    s := s.Parent;
  end;

  names := names.Remove(names.Length - 1);

  if names.IndexOf('=') = -1 then
    names := copy(names, 1)
  else
    names := copy(names, 1, names.IndexOf('=') + 1);
  Edit1.Text := names;

  GetString := names.Replace('|', '\').Replace('<', '').Replace('>', '').Replace('=', '\').Replace('Root\', '');
  if GetString.CountChar('\') > 1 then
  if not GetString.IsEmpty then
  if GetString[GetString.Length] <> '\' then
  begin
    GetString := GetString.Remove(LastDelimiter('\', GetString));
    GetString := GetString + 'Content';
  end;
  Edit2.Text := GetString;

  names := names.Replace('|<', '.Tags[''');
  names := names.Replace('>', ''']');

  names := names.Replace('|@', '.TagProperties.Values[''@');
  names := names.Replace('=', ''']');

  if names.Contains('|') then
  begin
    names := names.Remove(names.IndexOf('|'));
    names := names + '.TagContent';
  end;

  Edit1.Text := names;
  Edit1.CopyToClipboard;
end;

end.
