program XMLObjectParser;

uses
  Vcl.Forms,
  MainForm in 'MainForm.pas' {FormMain},
  XMLObject in 'XMLObject.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;
end.
