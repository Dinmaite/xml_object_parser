unit XMLObject;

interface

uses Classes, SysUtils, Generics.Collections, VCL.ComCtrls;

type
  TXMLObject = class
  private
    FMaxNestedDepth: Integer;

    function AddObject(const Value: string; var ArrayIndex: Integer): Integer;
    procedure AddParameter(const Value: string);

    function FindCloseSymbolIndex(const TagName, Value: string): Integer;

    procedure InternalParsXML(const XMLString: string;
      const MaxNestedDepth: Integer = 30);

    // Fill TreeView
    procedure SetContent(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: string);
    procedure SetParams(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TStringList);
    procedure SetObjects(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TObjectDictionary<string, TXMLObject>);
  public
    TagProperties: TStringList;
    Tags: TObjectDictionary<string, TXMLObject>;
    TagContent: string;
    function TagContentAsInteger: Integer;

    function GetValue(const Path: string): string;
    procedure SendToTreeView(TreeView: TTreeView);

    function ParsXML(const XMLString: string;
      const MaxNestedDepth: Integer = 30): Boolean;

    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TXMLObject }

function TXMLObject.AddObject(const Value: string;
  var ArrayIndex: Integer): Integer;
var
  CloseIndex, a, b: Integer;
  Temp: string;
  TempObject: TXMLObject;
  TagName, TagProperties, TagContent: string;

  TempContent: string;
  StartTempContent: Integer;
begin
  TagName := Value.Substring(1, Value.IndexOfAny([' ', '>']) - 1);

  if Value.IndexOf(' ') > 0 then
  begin
    TagProperties := Value.Substring(Value.IndexOf(' ') + 1,
      Value.IndexOfAny(['>']) - (Value.IndexOf(' ') + 1));
    if (not TagProperties.IsEmpty) and
      (TagProperties[TagProperties.Length] = '/') then
      TagProperties := TagProperties.Remove(TagProperties.Length - 1);
  end;

  a := Value.IndexOf('>', Value.IndexOf(TagName)) + 1;
  b := Value.IndexOf('/' + TagName);

  TagContent := Value.Substring(a, b - a - 1);
  while TagContent.Contains('&#') do
  begin
    StartTempContent := TagContent.IndexOf('&#');
    if StartTempContent = 0 then
      break;
    TempContent := TagContent.Substring(StartTempContent + 2,
      TagContent.IndexOf(';', StartTempContent) - StartTempContent - 2);
    try
      TagContent := TagContent.Replace('&#' + TempContent + ';',
        Chr(TempContent.ToInteger()));
    except
    end;
  end;
  Temp := Value;

  if Tags = nil then
    Tags := TObjectDictionary<string, TXMLObject>.Create([doOwnsValues]);
  CloseIndex := FindCloseSymbolIndex(TagName, Value);

  TempObject := TXMLObject.Create;
  TempObject.InternalParsXML(Copy(Temp, 2, CloseIndex - 2), FMaxNestedDepth);
  TempObject.AddParameter(TagProperties);
  if not TagContent.Contains('<') then
    TempObject.TagContent := TagContent;

  try
    Tags.Add(TagName, TempObject);
  except
    Tags.Add(TagName + '(' + ArrayIndex.ToString + ')', TempObject);
    Inc(ArrayIndex);
  end;

  Result := CloseIndex;
end;

procedure TXMLObject.AddParameter(const Value: string);
var
  PropertiesArray: TArray<string>;
  I: Integer;
begin
  if Value.IsEmpty then
    Exit;

  if TagProperties = nil then
    TagProperties := TStringList.Create;

  PropertiesArray := Value.Split([' ']);
  for I := 0 to Length(PropertiesArray) - 1 do
    TagProperties.Add('@' + PropertiesArray[I]);
end;

constructor TXMLObject.Create;
begin
  inherited Create;
end;

destructor TXMLObject.Destroy;
begin
  TagProperties.Free;
  Tags.Free;
  inherited;
end;

function TXMLObject.FindCloseSymbolIndex(const TagName, Value: string): Integer;
begin
  Result := -1;
  if Value.IsEmpty then
    Exit;

  Result := Value.IndexOf('/' + TagName + '>');
  if Result = -1 then
    Result := Value.IndexOf('/>');
end;

function TXMLObject.GetValue(const Path: string): string;
var
  LPath: TArray<string>;
  Node: TXMLObject;
  I: Integer;
begin
  Exit('NOWT WORK YET');
  LPath := Path.Split(['\']);

  Node := Self;
  for I := 0 to Length(LPath) - 1 do
  begin
    if Node.Tags <> nil then
      if Node.Tags.ContainsKey(LPath[I]) then
      begin
        Node := Node.Tags[LPath[I]];
        Continue;
      end;

    if Node.TagProperties <> nil then
    begin
      Result := Node.TagProperties.Values[LPath[I]];
      if not Result.IsEmpty then
        Exit;
    end;

    if not Node.TagContent.IsEmpty then
      Result := Node.TagContent;
  end;

  SetLength(LPath, 0);
end;

procedure TXMLObject.InternalParsXML(const XMLString: string;
  const MaxNestedDepth: Integer);
var
  ArrayIndex, I, ObjLength: Integer;
  LXMLString: string;
begin
  ArrayIndex := 0;
  LXMLString := XMLString;

  FMaxNestedDepth := MaxNestedDepth;
  Dec(FMaxNestedDepth);
  if FMaxNestedDepth < 1 then
    Exit;

  I := 0;

  while I < LXMLString.Length do
  begin
    Inc(I);
    if LXMLString[I] = '<' then
    begin
      ObjLength := AddObject(Copy(LXMLString, I, LXMLString.Length),
        ArrayIndex);
      if ObjLength > 1 then
        I := I + ObjLength;
    end;
  end;
end;

function TXMLObject.ParsXML(const XMLString: string;
  const MaxNestedDepth: Integer = 30): Boolean;
var
  LXMLString: string;
begin
  if Tags <> nil then
    Tags.Clear;
  LXMLString := XMLString.Replace('""', '"').Replace(#13#10, '')
    .Replace(#9, ' ');
  try
    InternalParsXML(LXMLString, MaxNestedDepth);
    Result := True;
  except
    Result := false;
  end;
end;

procedure TXMLObject.SendToTreeView(TreeView: TTreeView);
var
  Node: TTreeNode;
begin
  if TreeView = nil then
    Exit;
  Node := TreeView.Items.Add(nil, 'Root');
  SetObjects(TreeView, Node, Self.Tags);
end;

procedure TXMLObject.SetContent(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: string);
begin
  if not Obj.IsEmpty then
    TreeView.Items.AddChild(TreeNode, Obj);
end;

procedure TXMLObject.SetObjects(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TObjectDictionary<string, TXMLObject>);
var
  I: Integer;
  Node: TTreeNode;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
    begin
      Node := TreeView.Items.AddChild(TreeNode, '<' + Obj.ToArray[I].Key + '>');
      SetContent(TreeView, Node, Obj.ToArray[I].Value.TagContent);
      SetParams(TreeView, Node, Obj.ToArray[I].Value.TagProperties);
      SetObjects(TreeView, Node, Obj.ToArray[I].Value.Tags);
    end;
end;

procedure TXMLObject.SetParams(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TStringList);
var
  I: Integer;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
      TreeView.Items.AddChild(TreeNode, Obj[I])
end;

function TXMLObject.TagContentAsInteger: Integer;
begin
  try
    Result := TagContent.ToInteger();
  except
    Result := -1;
  end;
end;

end.